﻿using DataAccessLayerAbstraction.Mapping;
using DataAccessLayerAbstraction.Repositories;
using Npgsql;
using System;
using System.Collections.Generic;
using System.Data;
using System.Threading.Tasks;

namespace PgDataAccessLayer.Repositories
{
    public class PgEmployeeRepository : BasePgRepository, IEmployeeRepository
    {
        public PgEmployeeRepository(PgSettings settings) : base(settings) { }

        public async Task<int> Create(Employee obj)
        {
            if (string.IsNullOrEmpty(obj.FirstName))
            {
                throw new ArgumentNullException(nameof(obj.FirstName));
            }

            if (string.IsNullOrEmpty(obj.LastName))
            {
                throw new ArgumentNullException(nameof(obj.LastName));
            }

            if (obj.EmploymentDate == DateTime.MinValue || obj.EmploymentDate == DateTime.MaxValue)
            {
                throw new ArgumentNullException(nameof(obj.EmploymentDate));
            }

            if (obj.Job.Id <= 0)
            {
                throw new ArgumentOutOfRangeException(nameof(obj.Job.Id));
            }

            var connection = await GetOpenConnectionAsync();

            string command = "INSERT INTO Employee (firstName, lastName, employmentDate, job, accountId) " +
                             "VALUES(@firstName, @lastName, @employmentDate, @job, @accountId);";

            await using var cmd = new NpgsqlCommand(command, connection);

            cmd.Parameters.AddWithValue("@firstName", obj.FirstName);
            cmd.Parameters.AddWithValue("@lastName", obj.LastName);
            cmd.Parameters.AddWithValue("@employmentDate", obj.EmploymentDate);
            cmd.Parameters.AddWithValue("@job", obj.Job.Id);
            cmd.Parameters.AddWithValue("@accountId", obj.AccountId);

            return Convert.ToInt32(await cmd.ExecuteScalarAsync());
        }

        public async Task Update(Employee obj)
        {
            if (obj.Id <= 0)
            {
                throw new ArgumentOutOfRangeException(nameof(obj.Id));
            }

            if (obj.EmploymentDate.HasValue && obj.EmploymentDate == DateTime.MinValue || obj.EmploymentDate == DateTime.MaxValue)
            {
                throw new ArgumentNullException(nameof(obj.EmploymentDate));
            }

            if (obj.Job != null && obj.Job.Id <= 0)
            {
                throw new ArgumentOutOfRangeException(nameof(obj.Job.Id));
            }

            var connection = await GetOpenConnectionAsync();

            string command = "UPDATE Employee " +
                             "SET " +
                             $"{(string.IsNullOrEmpty(obj.FirstName) == false ? "firstName=@firstName " : string.Empty)}" +
                             $"{(string.IsNullOrEmpty(obj.LastName) == false ? "lastName=@lastName " : string.Empty)}" +
                             $"{(obj.EmploymentDate.HasValue ? "employmentDate=@employmentDate " : string.Empty)}" +
                             $"{(obj.Job != null ? "job=@job " : string.Empty)}" +
                             $"{(obj.AccountId.HasValue ? "accountId=@accountId " : string.Empty)}" +
                             "WHERE id = @id;";

            await using var cmd = new NpgsqlCommand(command, connection);

            cmd.Parameters.AddWithValue("@id", obj.Id);
            cmd.Parameters.AddWithValue("@firstName", obj.FirstName);
            cmd.Parameters.AddWithValue("@lastName", obj.LastName);
            cmd.Parameters.AddWithValue("@employmentDate", obj.EmploymentDate);
            cmd.Parameters.AddWithValue("@job", obj.Job?.Id);
            cmd.Parameters.AddWithValue("@accountId", obj.AccountId);
        }

        public async Task Delete(int id)
        {
            if (id <= 0)
            {
                throw new ArgumentOutOfRangeException(nameof(id));
            }

            var connection = await GetOpenConnectionAsync();

            string command = "DELETE FROM Employee " +
                             "WHERE id = @id;";

            await using var cmd = new NpgsqlCommand(command, connection);
            cmd.Parameters.AddWithValue("@id", id);

            await cmd.ExecuteNonQueryAsync();
        }

        public async Task<Employee> GetById(int id)
        {
            if (id <= 0)
            {
                throw new ArgumentOutOfRangeException(nameof(id));
            }

            var connection = await GetOpenConnectionAsync();

            string command = "SELECT Employee.id , firstName, lastName, employmentDate, job as jobId, title , accountId FROM Employee " +
                             "LEFT JOIN job as j ON Employee.job = j.id " +
                             "WHERE Employee.id = @id" +
                             "limit 1;";

            await using (var cmd = new NpgsqlCommand(command, connection))
            {
                cmd.Parameters.AddWithValue("@id", id);

                await using var reader = await cmd.ExecuteReaderAsync();

                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        return ParseEmployeeFromReader(reader);
                    }
                }
            }

            return null;
        }

        public async Task<List<Project>> GetProjects(int employeeId)
        {
            if (employeeId <= 0)
            {
                throw new ArgumentOutOfRangeException(nameof(employeeId));
            }

            var connection = await GetOpenConnectionAsync();

            string command = "SELECT id, title, description, startDate, releaseDate FROM ProjectEmployee" +
                             "LEFT JOIN Project as Pr ON Pr.id=ProjectEmployee.projectId" +
                             "WHERE ProjectEmployee.employeeId = @employeeId";

            List<Project> projects = new List<Project>();

            await using (var cmd = new NpgsqlCommand(command, connection))
            {
                cmd.Parameters.AddWithValue("@employeeId", employeeId);

                await using var reader = await cmd.ExecuteReaderAsync();

                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        projects.Add(ParseProjectFromReader(reader));
                    }
                }
            }

            return projects;
        }

        public async Task<List<Employee>> GetEmployes()
        {
            var connection = await GetOpenConnectionAsync();

            string command = "SELECT Employee.id , firstName, lastName, employmentDate, job as jobId, title , accountId FROM Employee " +
                             "LEFT JOIN job as j ON Employee.job = j.id;";

            List<Employee> employees = new List<Employee>();

            await using (var cmd = new NpgsqlCommand(command, connection))
            {
                await using var reader = await cmd.ExecuteReaderAsync();

                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        employees.Add(ParseEmployeeFromReader(reader));
                    }
                }
            }

            return employees;
        }

        #region private methods
        private Project ParseProjectFromReader(IDataReader reader)
        {
            return new Project
            {
                Id = reader.GetInt32(reader.GetOrdinal("id")),
                Title = reader.GetString(reader.GetOrdinal("title")),
                Description = reader.GetString(reader.GetOrdinal("description")),
                StartDate = reader.GetDateTime(reader.GetOrdinal("startDate")),
                ReleaseDate = reader.GetDateTime(reader.GetOrdinal("releaseDate"))
            };
        }

        private Employee ParseEmployeeFromReader(IDataReader reader)
        {
            return new Employee
            {
                Id = reader.GetInt32(reader.GetOrdinal("id")),
                FirstName = reader.GetString(reader.GetOrdinal("firstName")),
                LastName = reader.GetString(reader.GetOrdinal("lastName")),
                Job = new Job()
                {
                    Id = reader.GetInt32(reader.GetOrdinal("jobId")),
                    Title = reader.GetString(reader.GetOrdinal("jobTitle"))
                },
                EmploymentDate = reader.GetDateTime(reader.GetOrdinal("employmentDate")),
                AccountId = reader.GetInt32(reader.GetOrdinal("accountId"))
            };
        }
        #endregion
    }
}
