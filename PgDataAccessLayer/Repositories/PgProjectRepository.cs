﻿using DataAccessLayerAbstraction.Mapping;
using DataAccessLayerAbstraction.Repositories;
using Npgsql;
using System;
using System.Collections.Generic;
using System.Data;
using System.Threading.Tasks;

namespace PgDataAccessLayer.Repositories
{
    public class PgProjectRepository : BasePgRepository, IProjectRepository
    {
        public PgProjectRepository(PgSettings settings) : base(settings) { }

        public async Task<int> Create(Project obj)
        {
            if (string.IsNullOrEmpty(obj.Title))
            {
                throw new ArgumentNullException(nameof(obj.Title));
            }

            if (string.IsNullOrEmpty(obj.Description))
            {
                throw new ArgumentNullException(nameof(obj.Description));
            }

            if (obj.StartDate == DateTime.MinValue || obj.StartDate == DateTime.MaxValue)
            {
                throw new ArgumentNullException(nameof(obj.StartDate));
            }

            if (obj.ReleaseDate == DateTime.MinValue || obj.ReleaseDate == DateTime.MaxValue)
            {
                throw new ArgumentOutOfRangeException(nameof(obj.ReleaseDate));
            }

            if (obj.ReleaseDate <= obj.StartDate)
            {
                throw new ArgumentOutOfRangeException($"{obj.ReleaseDate} can't be less or equal {obj.StartDate}");
            }

            var connection = await GetOpenConnectionAsync();

            string command = "INSERT INTO Project (title, description, startDate, releaseDate) " +
                             "VALUES(@title, @description, @startDate, @releaseDate);";

            await using var cmd = new NpgsqlCommand(command, connection);

            cmd.Parameters.AddWithValue("@title", obj.Title);
            cmd.Parameters.AddWithValue("@description", obj.Description);
            cmd.Parameters.AddWithValue("@startDate", obj.StartDate);
            cmd.Parameters.AddWithValue("@releaseDate", obj.ReleaseDate);

            return Convert.ToInt32(await cmd.ExecuteScalarAsync());
        }

        public async Task Update(Project obj)
        {
            if (obj.Id <= 0)
            {
                throw new ArgumentOutOfRangeException(nameof(obj.Id));
            }
            if (obj.StartDate.HasValue && obj.StartDate == DateTime.MinValue || obj.StartDate == DateTime.MaxValue)
            {
                throw new ArgumentNullException(nameof(obj.StartDate));
            }

            if (obj.StartDate.HasValue && obj.ReleaseDate == DateTime.MinValue || obj.ReleaseDate == DateTime.MaxValue)
            {
                throw new ArgumentOutOfRangeException(nameof(obj.ReleaseDate));
            }

            var connection = await GetOpenConnectionAsync();

            string command = "UPDATE Project " +
                             "SET " +
                             $"{(string.IsNullOrEmpty(obj.Title) == false ? "title=@title " : string.Empty)}" +
                             $"{(string.IsNullOrEmpty(obj.Description) == false ? "description=@description " : string.Empty)}" +
                             $"{(obj.StartDate.HasValue ? "startDate=@StartDate " : string.Empty)}" +
                             $"{(obj.ReleaseDate.HasValue ? "releaseDate=@ReleaseDate " : string.Empty)}" +
                             "WHERE id = @id;";

            await using var cmd = new NpgsqlCommand(command, connection);

            cmd.Parameters.AddWithValue("@id", obj.Id);
            cmd.Parameters.AddWithValue("@title", obj.Title);
            cmd.Parameters.AddWithValue("@description", obj.Description);
            cmd.Parameters.AddWithValue("@startDate", obj.StartDate);
            cmd.Parameters.AddWithValue("@releaseDate", obj.ReleaseDate);

            await cmd.ExecuteNonQueryAsync();
        }

        public async Task Delete(int id)
        {
            if (id <= 0)
            {
                throw new ArgumentOutOfRangeException(nameof(id));
            }

            var connection = await GetOpenConnectionAsync();

            string command = "DELETE FROM Project " +
                             "WHERE id = @id;";

            await using var cmd = new NpgsqlCommand(command, connection);
            cmd.Parameters.AddWithValue("@id", id);

            await cmd.ExecuteNonQueryAsync();
        }

        public async Task<Project> GetById(int id)
        {
            if (id <= 0)
            {
                throw new ArgumentOutOfRangeException(nameof(id));
            }

            var connection = await GetOpenConnectionAsync();

            string command = "SELECT  id, title, description, startDate, releaseDate," +
                             "(SELECT COUNT(*) FROM ProjectEmployee WHERE projectId=Project.id) " +
                             "FROM Project" +
                             "WHERE Project.id = @id" +
                             "limit 1;";

            await using (var cmd = new NpgsqlCommand(command, connection))
            {
                cmd.Parameters.AddWithValue("@id", id);

                await using var reader = await cmd.ExecuteReaderAsync();

                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        return ParseProjectFromReader(reader);
                    }
                }
            }

            return null;
        }

        public async Task<List<Project>> GetProjects()
        {
            var connection = await GetOpenConnectionAsync();

            string command = "SELECT  id, title, description, startDate, releaseDate," +
                             "(SELECT COUNT(*) FROM ProjectEmployee WHERE projectId=Project.id) " +
                             "FROM Project ;";

            List<Project> project = new List<Project>();

            await using (var cmd = new NpgsqlCommand(command, connection))
            {
                await using var reader = await cmd.ExecuteReaderAsync();

                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        project.Add(ParseProjectFromReader(reader));
                    }
                }
            }

            return project;
        }

        public async Task AddEmployee(int projectId, int employeeId)
        {
            if (projectId <= 0)
            {
                throw new ArgumentOutOfRangeException(nameof(projectId));
            }

            if (employeeId <= 0)
            {
                throw new ArgumentOutOfRangeException(nameof(employeeId));
            }

            var connection = await GetOpenConnectionAsync();

            string command = "INSERT INTO ProjectEmployee (projectId, employeeId)" +
                             "VALUES (@projectId,@employeeId);";

            await using var cmd = new NpgsqlCommand(command, connection);
            cmd.Parameters.AddWithValue("@projectId", projectId);
            cmd.Parameters.AddWithValue("@employeeId", employeeId);

            await cmd.ExecuteNonQueryAsync();
        }

        public async Task RemoveEmployee(int projectId, int employeeId)
        {
            if (projectId <= 0)
            {
                throw new ArgumentOutOfRangeException(nameof(projectId));
            }

            if (employeeId <= 0)
            {
                throw new ArgumentOutOfRangeException(nameof(employeeId));
            }

            var connection = await GetOpenConnectionAsync();

            string command = "DELETE FROM ProjectEmployee" +
                             "VALUES (@projectId, @employeeId);";

            await using var cmd = new NpgsqlCommand(command, connection);
            cmd.Parameters.AddWithValue("@projectId", projectId);
            cmd.Parameters.AddWithValue("@employeeId", employeeId);

            await cmd.ExecuteNonQueryAsync();
        }

        public async Task<List<Employee>> GetEmployes(int projectId)
        {
            if (projectId <= 0)
            {
                throw new ArgumentOutOfRangeException(nameof(projectId));
            }

            var connection = await GetOpenConnectionAsync();

            string command = "SELECT Employee.id , firstName, lastName, employmentDate, job as jobId, title , accountId FROM ProjectEmployee " +
                             "LEFT JOIN Employee ON Employee.id = ProjectEmployee.employeeID " +
                             "LEFT JOIN Job On Job.Id = Employee.job" +
                             "WHERE projectId = @projectId";

            List<Employee> employees = new List<Employee>();

            await using (var cmd = new NpgsqlCommand(command, connection))
            {
                cmd.Parameters.AddWithValue("@projectId", projectId);
                await using var reader = await cmd.ExecuteReaderAsync();

                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        employees.Add(ParseEmployeeFromReader(reader));
                    }
                }
            }

            return employees;
        }

        #region private methods
        private Project ParseProjectFromReader(IDataReader reader)
        {
            return new Project
            {
                Id = reader.GetInt32(reader.GetOrdinal("id")),
                Title = reader.GetString(reader.GetOrdinal("title")),
                Description = reader.GetString(reader.GetOrdinal("description")),
                StartDate = reader.GetDateTime(reader.GetOrdinal("startDate")),
                ReleaseDate = reader.GetDateTime(reader.GetOrdinal("releaseDate")),
                EmployeesCount = reader.GetInt32(reader.GetOrdinal("count"))
            };
        }

        private Employee ParseEmployeeFromReader(IDataReader reader)
        {
            return new Employee
            {
                Id = reader.GetInt32(reader.GetOrdinal("id")),
                FirstName = reader.GetString(reader.GetOrdinal("firstName")),
                LastName = reader.GetString(reader.GetOrdinal("lastName")),
                Job = new Job()
                {
                    Id = reader.GetInt32(reader.GetOrdinal("jobId")),
                    Title = reader.GetString(reader.GetOrdinal("jobTitle"))
                },
                EmploymentDate = reader.GetDateTime(reader.GetOrdinal("employmentDate")),
                AccountId = reader.GetInt32(reader.GetOrdinal("accountId"))
            };
        }
        #endregion
    }
}
