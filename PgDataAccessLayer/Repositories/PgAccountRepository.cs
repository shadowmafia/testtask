﻿using DataAccessLayerAbstraction.Mapping;
using DataAccessLayerAbstraction.Mapping.Types;
using DataAccessLayerAbstraction.Repositories;
using Npgsql;
using System;
using System.Data;
using System.Threading.Tasks;

namespace PgDataAccessLayer.Repositories
{
    public class PgAccountRepository : BasePgRepository, IAccountRepository
    {
        public PgAccountRepository(PgSettings settings) : base(settings) { }

        public async Task<int> Create(Account obj)
        {
            if (string.IsNullOrEmpty(obj.Login))
            {
                throw new ArgumentNullException(nameof(obj.Login));
            }

            if (string.IsNullOrEmpty(obj.Password))
            {
                throw new ArgumentNullException(nameof(obj.Password));
            }

            var connection = await GetOpenConnectionAsync();

            string command = "INSERT INTO Account (login, password, role) " +
                             "VALUES (@login, @password, @role);" +
                             "SELECT currval(pg_get_serial_sequence('Account','id'));";

            await using var cmd = new NpgsqlCommand(command, connection);
            cmd.Parameters.AddWithValue("@login", obj.Login);
            cmd.Parameters.AddWithValue("@password", obj.Password);
            cmd.Parameters.AddWithValue("@role", (short)obj.Role);

            return Convert.ToInt32(await cmd.ExecuteScalarAsync());
        }

        public async Task Delete(int id)
        {
            if (id <= 0)
            {
                throw new ArgumentOutOfRangeException(nameof(id));
            }

            var connection = await GetOpenConnectionAsync();

            string command = "DELETE FROM Account " +
                             "WHERE id=@id;";

            await using var cmd = new NpgsqlCommand(command, connection);
            cmd.Parameters.AddWithValue("@id", id);

            await cmd.ExecuteNonQueryAsync();
        }

        public async Task<Account> GetById(int id)
        {
            if (id <= 0)
            {
                throw new ArgumentOutOfRangeException(nameof(id));
            }

            var connection = await GetOpenConnectionAsync();

            string command = "SELECT id, login, password, role FROM Account " +
                             "WHERE id=@id " +
                             "limit 1;";

            await using (var cmd = new NpgsqlCommand(command, connection))
            {
                cmd.Parameters.AddWithValue("@id", id);
                await using var reader = await cmd.ExecuteReaderAsync();

                if (reader.HasRows)
                {
                    return ParseAccountFromReader(reader);
                }
            }

            return null;
        }

        public async Task<Account> GetByLogin(string login)
        {
            if (string.IsNullOrEmpty(login))
            {
                throw new ArgumentNullException(nameof(login));
            }

            var connection = await GetOpenConnectionAsync();

            string command = "SELECT id, login, password, role FROM Account " +
                             "WHERE login=@login " +
                             "limit 1;";

            await using (var cmd = new NpgsqlCommand(command, connection))
            {
                cmd.Parameters.AddWithValue("@login", login);
                await using var reader = await cmd.ExecuteReaderAsync();

                if (reader.HasRows)
                {
                    while (await reader.ReadAsync())
                    {
                        return ParseAccountFromReader(reader);
                    }
                }
            }

            return null;
        }

        public async Task UpdatePassword(int accountId, string password)
        {
            if (accountId <= 0)
            {
                throw new ArgumentOutOfRangeException(nameof(accountId));
            }

            if (string.IsNullOrEmpty(password))
            {
                throw new ArgumentNullException();
            }

            var connection = await GetOpenConnectionAsync();

            string command = "UPDATE Account " +
                             "SET role = @passowrd" +
                             "WHERE id = @id;";

            await using var cmd = new NpgsqlCommand(command, connection);

            cmd.Parameters.AddWithValue("@id", accountId);
            cmd.Parameters.AddWithValue("@password", password);
            await cmd.ExecuteNonQueryAsync();
        }

        public async Task UpdateRole(int accountId, Role role)
        {
            if (accountId <= 0)
            {
                throw new ArgumentOutOfRangeException(nameof(accountId));
            }

            var connection = await GetOpenConnectionAsync();

            string command = "UPDATE Account " +
                             "SET role = @role" +
                             "WHERE id = @id;";

            await using var cmd = new NpgsqlCommand(command, connection);

            cmd.Parameters.AddWithValue("@id", accountId);
            cmd.Parameters.AddWithValue("@role", (short)role);
            await cmd.ExecuteNonQueryAsync();
        }

        #region private methods
        private Account ParseAccountFromReader(IDataReader reader)
        {
            return new Account
            {
                Id = reader.GetInt32(reader.GetOrdinal("id")),
                Login = reader.GetString(reader.GetOrdinal("login")),
                Password = reader.GetString(reader.GetOrdinal("password")),
                Role = (Role)char.GetNumericValue(reader.GetChar(reader.GetOrdinal("role")))
            };
        }
        #endregion
    }
}
