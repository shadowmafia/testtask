﻿using DataAccessLayerAbstraction.Mapping;
using DataAccessLayerAbstraction.Repositories;
using Npgsql;
using System;
using System.Data;
using System.Threading.Tasks;

namespace PgDataAccessLayer.Repositories
{
    public class PgJobRepository : BasePgRepository, IJobRepository
    {
        public PgJobRepository(PgSettings settings) : base(settings) { }

        public async Task<int> Create(Job obj)
        {
            if (string.IsNullOrEmpty(obj.Title))
            {
                throw new ArgumentNullException(nameof(obj.Title));
            }

            var connection = await GetOpenConnectionAsync();

            string command = "INSERT INTO Job (title) " +
                             "VALUES (@title);" +
                             "SELECT currval(pg_get_serial_sequence('Job','id'));";

            await using var cmd = new NpgsqlCommand(command, connection);

            cmd.Parameters.AddWithValue("@title", obj.Title);

            return Convert.ToInt32(await cmd.ExecuteScalarAsync());
        }

        public async Task Delete(int id)
        {
            if (id <= 0)
            {
                throw new ArgumentOutOfRangeException(nameof(id));
            }

            var connection = await GetOpenConnectionAsync();

            string command = "DELETE FROM Job " +
                             "WHERE id = @id;";

            await using var cmd = new NpgsqlCommand(command, connection);
            cmd.Parameters.AddWithValue("@id", id);

            await cmd.ExecuteNonQueryAsync();
        }

        public async Task<Job> GetById(int id)
        {
            if (id <= 0)
            {
                throw new ArgumentOutOfRangeException(nameof(id));
            }

            var connection = await GetOpenConnectionAsync();

            string command = "SELECT id, title FROM Job " +
                             "WHERE id = @id " +
                             "limit 1;";

            await using (var cmd = new NpgsqlCommand(command, connection))
            {
                cmd.Parameters.AddWithValue("@id", id);
                await using var reader = await cmd.ExecuteReaderAsync();

                if (reader.HasRows)
                {
                    return ParseJobFromReader(reader);
                }
            }

            return null;
        }

        public async Task Update(Job obj)
        {
            if (obj.Id <= 0)
            {
                throw new ArgumentOutOfRangeException(nameof(obj.Id));
            }

            var connection = await GetOpenConnectionAsync();

            string command = "UPDATE Job " +
                             "Set title=@title; ";

            await using var cmd = new NpgsqlCommand(command, connection);
            cmd.Parameters.AddWithValue("@id", obj.Id);
            cmd.Parameters.AddWithValue("@title", obj.Title);
            await cmd.ExecuteNonQueryAsync();
        }

        #region private methods
        private Job ParseJobFromReader(IDataReader reader)
        {
            return new Job
            {
                Id = reader.GetInt32(reader.GetOrdinal("id")),
                Title = reader.GetString(reader.GetOrdinal("title")),
            };
        }
        #endregion
    }
}
