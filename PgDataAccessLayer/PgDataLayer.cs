﻿using DataAccessLayerAbstraction;
using DataAccessLayerAbstraction.Repositories;
using PgDataAccessLayer.Repositories;
using System;

namespace PgDataAccessLayer
{
    public class PgDataAccessLayer : IDataAccessLayer
    {
        public IAccountRepository Account { get; }
        public IEmployeeRepository Employee { get => throw new NotImplementedException(); set => throw new NotImplementedException(); }
        public IJobRepository Job { get => throw new NotImplementedException(); set => throw new NotImplementedException(); }
        public IProjectRepository Project { get => throw new NotImplementedException(); set => throw new NotImplementedException(); }

        public PgDataAccessLayer(PgSettings settings)
        {
            Account = new PgAccountRepository(settings);
        }
    }
}
