﻿using Npgsql;
using System;
using System.Threading.Tasks;

namespace PgDataAccessLayer
{
    public abstract class BasePgRepository
    {
        private readonly PgSettings _pgSettings;

        protected BasePgRepository(PgSettings settings)
        {
            _pgSettings = settings ?? throw new ArgumentNullException(nameof(settings));

            if (string.IsNullOrEmpty(settings.ConnectionString))
            {
                throw new ArgumentException(nameof(settings.ConnectionString));
            }
        }

        protected async Task<NpgsqlConnection> GetOpenConnectionAsync()
        {
            var connection = new NpgsqlConnection(_pgSettings.ConnectionString);
            await connection.OpenAsync();

            return connection;
        }
    }
}
