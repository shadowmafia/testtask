﻿using AutoMapper;
using Microsoft.Extensions.DependencyInjection;
using WebApi.Auth;
using WebApi.Mappers;

namespace WebApi
{
    public static class DiExtensions
    {
        public static void AddAuthServices(this IServiceCollection services)
        {
            services.AddSingleton<IPasswordHasher, PasswordHasher>();
            services.AddScoped<IUserInfo, UserInfo>();
            services.AddSingleton<IAuthenticationService, AuthenticationService>();
        }

        public static void AddMappers(this IServiceCollection service)
        {
            var mapperConfig = new MapperConfiguration(mc =>
            {
                mc.AddProfile(new FromAuthDto());
                mc.AddProfile(new ToAccount());
                mc.AddProfile(new ToEmployee());
                mc.AddProfile(new ToProject());
            });

            IMapper mapper = mapperConfig.CreateMapper();
            service.AddSingleton(mapper);
        }
    }
}
