﻿using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;
using WebApi.Auth;
using WebApi.Auth.Models;
using WebApi.Models.Authorization;

namespace WebApi.Controllers
{
    [ApiVersion("0.0")]
    [Route("api/v{version:apiVersion}/auth")]
    [ApiController]
    public class AuthController : ControllerBase
    {
        private readonly IAuthenticationService _authenticationService;
        private readonly IMapper _mapper;

        public AuthController(IAuthenticationService authenticationService, IMapper mapper)
        {
            _authenticationService = authenticationService ?? throw new ArgumentNullException(nameof(authenticationService));
            _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
        }

        [HttpPost]
        public async Task<ActionResult<AuthResponse>> GetAccessToken(AuthDto dtoData)
        {
            string token = await _authenticationService.Authorize(
               userLoginModel: new UserLoginModel
               {
                   Login = dtoData.Login,
                   Password = dtoData.Password
               },
               tokenLifeTime: TimeSpan.FromHours(12)
            );

            if (string.IsNullOrEmpty(token))
            {
                return BadRequest("User not found");
            }

            return Ok(new AuthResponse
            {
                UserId = dtoData.Login,
                Token = token
            });
        }
    }
}
