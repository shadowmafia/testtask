﻿using AutoMapper;
using DataAccessLayerAbstraction;
using DataAccessLayerAbstraction.Mapping;
using DataAccessLayerAbstraction.Mapping.Types;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using WebApi.Auth.Attributes;
using WebApi.Models.Project;

namespace WebApi.Controllers
{
    [Route("api/v0/[controller]")]
    [ApiController]
    [Auth]
    public class ProjectController : ControllerBase
    {
        private readonly IDataAccessLayer _dal;
        private readonly IMapper _mapper;

        public ProjectController(IDataAccessLayer dal, IMapper mapper)
        {
            _dal = dal ?? throw new ArgumentNullException(nameof(dal));
            _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
        }

        [HttpPost("create-project")]
        [Auth(Role.Admin)]
        public async Task<IActionResult> CreateProject(CreateProjectDto dto)
        {
            if (string.IsNullOrEmpty(dto.Title))
            {
                return BadRequest($"Incorrect parameter value {nameof(dto.Title)}");
            }

            if (string.IsNullOrEmpty(dto.Description))
            {
                return BadRequest($"Incorrect parameter value {nameof(dto.Description)}");
            }

            if (dto.ReleaseDate <= dto.StartDate)
            {
                return BadRequest($"ReleaseDate can't be less or equal StartDate");
            }

            await _dal.Project.Create(_mapper.Map<Project>(dto));

            return Ok($"Project was created {dto.Id}");
        }

        [HttpDelete("delete-project")]
        [Auth(Role.Admin)]
        public async Task<IActionResult> DeleteProject(int projectId)
        {
            if (projectId <= 0)
            {
                return BadRequest($"Incorrect parameter value {nameof(projectId)}");
            }

            await _dal.Project.Delete(projectId);

            return Ok($"Project with id {projectId} was delete ");
        }

        [HttpPost("update-project")]
        [Auth(Role.Admin)]
        public async Task<ActionResult> UpdateProject(UpdateProjectDto dto)
        {
            if (dto.Id <= 0)
            {
                return BadRequest($"Incorrect parameter value {nameof(dto.Id)}");
            }

            await _dal.Project.Update(_mapper.Map<Project>(dto));

            return Ok($"Project with id {dto.Id} was updated ");
        }

        [HttpGet("get-projects")]
        [AllowNotAuth]
        public async Task<ActionResult<List<Project>>> GetProjects()
        {
            return await _dal.Project.GetProjects();
        }

        [HttpGet("days-left")]
        [AllowNotAuth]
        public async Task<ActionResult<TimeSpan>> DaysLeft(int projectId)
        {
            var project = await _dal.Project.GetById(projectId);

            return (project.ReleaseDate.Value - project.StartDate.Value);
        }
    }
}
