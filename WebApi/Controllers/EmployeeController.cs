﻿using AutoMapper;
using DataAccessLayerAbstraction;
using DataAccessLayerAbstraction.Mapping;
using DataAccessLayerAbstraction.Mapping.Types;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using WebApi.Auth.Attributes;
using WebApi.Models.Employee;

namespace WebApi.Controllers
{
    [ApiVersion("0.0")]
    [Route("api/v{version:apiVersion}/employe")]
    [ApiController]
    public class EmployeeController : ControllerBase
    {
        private readonly IDataAccessLayer _dal;
        private readonly IMapper _mapper;

        public EmployeeController(IDataAccessLayer dal, IMapper mapper)
        {
            _dal = dal ?? throw new ArgumentNullException(nameof(dal));
            _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
        }

        [HttpPost("create-employee")]
        [Auth(Role.Admin)]
        public async Task<IActionResult> CreateEmployee(CreateEmployeeDto dtoData)
        {
            if (string.IsNullOrEmpty(dtoData.FirstName))
            {
                return BadRequest($"Incorrect parameter value {nameof(dtoData.FirstName)}");
            }

            if (string.IsNullOrEmpty(dtoData.LastName))
            {
                return BadRequest($"Incorrect parameter value {nameof(dtoData.LastName)}");
            }

            if (dtoData.JobId <= 0)
            {
                return BadRequest($"Incorrect parameter value {nameof(dtoData.JobId)}");
            }

            if (dtoData.EmploymentDate == DateTime.MinValue || dtoData.EmploymentDate == DateTime.MaxValue)
            {
                return BadRequest($"Incorrect parameter value {nameof(dtoData.EmploymentDate)}");
            }

            int createdEmployeeId = await _dal.Employee.Create(_mapper.Map<Employee>(dtoData));

            return Ok($"Employee was created {createdEmployeeId}");
        }

        [HttpPatch("delete-employee")]
        [Auth(Role.Admin, Role.Employe)]
        public async Task<IActionResult> UpdateEmployee(UpdateEmployeeDto dtoData)
        {
            if (dtoData.Id <= 0)
            {
                return BadRequest($"Incorrect parameter value {nameof(dtoData.Id)}");
            }

            if (dtoData.JobId <= 0)
            {
                return BadRequest($"Incorrect parameter value {nameof(dtoData.JobId)}");
            }

            await _dal.Employee.Update(_mapper.Map<Employee>(dtoData));

            return Ok($"Employee was update {dtoData.Id}");
        }

        [HttpDelete("delete-employee")]
        [Auth(Role.Admin)]
        public async Task<IActionResult> DeleteEmployee(int employeeId)
        {
            if (employeeId <= 0)
            {
                return BadRequest($"Incorrect parameter value {nameof(employeeId)}");
            }

            await _dal.Employee.Delete(employeeId);

            return Ok($"Employee was delete {employeeId}");
        }

        [HttpGet("{employeeId}/project")]
        [Auth(Role.Admin, Role.Employe)]
        public async Task<ActionResult<List<Project>>> GetProjects(int employeeId)
        {
            if (employeeId <= 0)
            {
                return BadRequest($"Incorrect parameter value {nameof(employeeId)}");
            }

            return await _dal.Employee.GetProjects(employeeId);
        }

        [HttpPost("{employeeId}/project/assign")]
        [Auth(Role.Admin)]
        public async Task<IActionResult> AssignToProject(int employeeId, [FromBody] int projectId)
        {
            if (employeeId <= 0)
            {
                return BadRequest($"Incorrect parameter value {nameof(employeeId)}");
            }

            if (projectId <= 0)
            {
                return BadRequest($"Incorrect parameter value {nameof(employeeId)}");
            }

            await _dal.Project.AddEmployee(projectId, employeeId);

            return Ok($"Employee:{employeeId} was Assigned to Project:{projectId}");
        }

        [HttpPost("{employeeId}/project/unassign")]
        [Auth(Role.Admin)]
        public async Task<IActionResult> UnassignToProject(int employeeId, [FromBody] int projectId)
        {
            if (employeeId <= 0)
            {
                return BadRequest($"Incorrect parameter value {nameof(employeeId)}");
            }

            if (projectId <= 0)
            {
                return BadRequest($"Incorrect parameter value {nameof(employeeId)}");
            }

            await _dal.Project.RemoveEmployee(projectId, employeeId);

            return Ok($"Employee:{employeeId} was Assigned to Project:{projectId}");
        }

        [HttpGet("get-employes")]
        [Auth(Role.Admin)]
        public async Task<ActionResult<List<Employee>>> GetEmployes()
        {
            return await _dal.Employee.GetEmployes();
        }
    }
}