﻿using DataAccessLayerAbstraction;
using DataAccessLayerAbstraction.Mapping;
using DataAccessLayerAbstraction.Mapping.Types;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;
using WebApi.Auth.Attributes;

namespace WebApi.Controllers
{
    [ApiVersion("0.0")]
    [Route("api/v{version:apiVersion}/job")]
    [ApiController]
    public class JobController : ControllerBase
    {
        private readonly IDataAccessLayer _dal;

        public JobController(IDataAccessLayer dal)
        {
            _dal = dal ?? throw new ArgumentNullException(nameof(dal));
        }

        [HttpPost("create-job")]
        [Auth(Role.Admin)]
        public async Task<IActionResult> CreateJob(string title)
        {
            if (string.IsNullOrEmpty(title))
            {
                return BadRequest($"Incorrect parameter value {nameof(title)}");
            }

            int createJobId = await _dal.Job.Create(new Job
            {
                Title = title
            });

            return Ok($"Job was created id: {createJobId}");
        }

        [HttpDelete("delete-job")]
        [Auth(Role.Admin)]
        public async Task<IActionResult> DeleteJob(int jobId)
        {
            if (jobId <= 0)
            {
                return BadRequest($"Incorrect parameter value {nameof(jobId)}");
            }

            await _dal.Job.Delete(jobId);

            return Ok($"Job with id:{jobId} was deleted");
        }

        [HttpPut("update-job")]
        [Auth(Role.Employe)]
        public async Task<IActionResult> UpdateJob(int jobId, string title)
        {
            if (jobId <= 0)
            {
                return BadRequest($"Incorrect parameter value {nameof(jobId)}");
            }

            if (string.IsNullOrEmpty(title))
            {
                return BadRequest($"Incorrect parameter value {nameof(title)}");
            }

            await _dal.Job.Update(new Job
            {
                Id = jobId,
                Title = title
            });

            return Ok($"Job with id:{jobId} was updated");
        }
    }
}
