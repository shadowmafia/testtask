﻿using AutoMapper;
using DataAccessLayerAbstraction;
using DataAccessLayerAbstraction.Mapping;
using DataAccessLayerAbstraction.Mapping.Types;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;
using WebApi.Auth.Attributes;
using WebApi.Models.Account;

namespace WebApi.Controllers
{
    [ApiVersion("0.0")]
    [Route("api/v{version:apiVersion}/account")]
    [ApiController]
    [Auth]
    public class AccountController : ControllerBase
    {
        private readonly IDataAccessLayer _dal;
        private readonly IMapper _mapper;

        public AccountController(IDataAccessLayer dal, IMapper mapper)
        {
            _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
            _dal = dal ?? throw new ArgumentNullException(nameof(dal));
        }

        [HttpPost("create-account")]
        [Auth(Role.Admin)]
        public async Task<IActionResult> CreateAccount(CreateAccountDto dtoData)
        {
            if (string.IsNullOrEmpty(dtoData.Login))
            {
                return BadRequest();
            }

            if (string.IsNullOrEmpty(dtoData.Password))
            {
                return BadRequest();
            }

            await _dal.Account.Create(_mapper.Map<Account>(dtoData));

            return Ok();
        }

        [HttpDelete("delete-account")]
        [Auth(Role.Admin)]
        public async Task<IActionResult> DeleteAccount(int accountId)
        {
            if (accountId <= 0)
            {
                return BadRequest();
            }

            await _dal.Account.Delete(accountId);

            return Ok();
        }
    }
}
