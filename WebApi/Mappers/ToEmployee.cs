﻿using AutoMapper;
using DataAccessLayerAbstraction.Mapping;
using WebApi.Models.Employee;

namespace WebApi.Mappers
{
    public class ToEmployee : Profile
    {
        public ToEmployee()
        {
            CreateMap<CreateEmployeeDto, Employee>()
                .ForMember(
                    employee => employee.Job.Id,
                    opt => opt.MapFrom(ps => ps.JobId)
                );
            CreateMap<UpdateEmployeeDto, Employee>()
                .ForMember(
                    employee => employee.Job.Id,
                    opt => opt.MapFrom(ps => ps.JobId)
                );
        }
    }
}
