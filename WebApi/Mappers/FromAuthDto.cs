﻿using AutoMapper;
using WebApi.Auth.Models;
using WebApi.Models.Authorization;

namespace WebApi.Mappers
{
    public class FromAuthDto : Profile
    {
        public FromAuthDto()
        {
            CreateMap<AuthDto, UserLoginModel>();
        }
    }
}
