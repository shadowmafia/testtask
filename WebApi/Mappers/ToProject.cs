﻿using AutoMapper;
using DataAccessLayerAbstraction.Mapping;
using WebApi.Models.Project;

namespace WebApi.Mappers
{
    public class ToProject : Profile
    {
        public ToProject()
        {
            CreateMap<CreateProjectDto, Project>();
            CreateMap<UpdateProjectDto, Project>();
        }
    }
}

