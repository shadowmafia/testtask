﻿using AutoMapper;
using DataAccessLayerAbstraction.Mapping;
using WebApi.Models.Account;

namespace WebApi.Mappers
{
    public class ToAccount : Profile
    {
        public ToAccount()
        {
            CreateMap<CreateAccountDto, Account>();
        }
    }
}
