using DataAccessLayerAbstraction;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using PgDataAccessLayer;
using WebApi.Auth;
using WebApi.Auth.Settings;

namespace WebApi
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllers();

            var authSettingsSection = Configuration.GetSection("AuthorizationSettings");
            services.Configure<AuthSettings>(
                authSettingsSection
            );

            var dataLayerSettings = Configuration.GetSection("PgSettings");
            services.Configure<PgSettings>(
                authSettingsSection
            );

            PgSettings pgSettings = new PgSettings();
            dataLayerSettings.Bind(pgSettings);

            services.AddApiVersioning();
            services.AddSingleton<IDataAccessLayer>(new PgDataAccessLayer.PgDataAccessLayer(pgSettings));
            services.AddAuthServices();
            services.AddMappers();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env, IPasswordHasher passwordHasher)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseCors(x => x
                .AllowAnyOrigin()
                .AllowAnyMethod()
                .AllowAnyHeader());

            app.UseMiddleware<AuthorizationMiddleware>();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
