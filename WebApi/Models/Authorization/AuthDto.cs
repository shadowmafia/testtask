﻿namespace WebApi.Models.Authorization
{
    public class AuthDto
    {
        public string Login { get; set; }
        public string Password { get; set; }
    }
}
