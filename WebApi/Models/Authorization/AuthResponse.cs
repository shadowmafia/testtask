﻿namespace WebApi.Models.Authorization
{
    public class AuthResponse
    {
        public string UserId { get; set; }
        public string Token { get; set; }
    }
}
