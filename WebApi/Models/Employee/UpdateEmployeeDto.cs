﻿using System;

namespace WebApi.Models.Employee
{
    public class UpdateEmployeeDto
    {
        public int Id { get; set; }
        public int? AccountId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public DateTime? EmploymentDate { get; set; }
        public int? JobId { get; set; }
    }
}
