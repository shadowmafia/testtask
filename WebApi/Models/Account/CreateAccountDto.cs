﻿using DataAccessLayerAbstraction.Mapping.Types;

namespace WebApi.Models.Account
{
    public class CreateAccountDto
    {
        public string Login { get; set; }
        public string Password { get; set; }
        public Role Role { get; set; }
    }
}
