﻿using DataAccessLayerAbstraction.Mapping.Types;

namespace WebApi.Auth.Models
{
    public class CurrentUser
    {
        public string UserId { get; set; }
        public Role Role { get; set; }
    }
}
