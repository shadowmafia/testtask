﻿using WebApi.Auth.Models;

namespace WebApi.Auth
{
    public interface IUserInfo
    {
        bool IsAuthorize { get; }
        CurrentUser User { get; }
        void Set(CurrentUser user);
        void Clean(CurrentUser user);
    }

    public class UserInfo : IUserInfo
    {
        public bool IsAuthorize { get; private set; }
        public CurrentUser User { get; private set; }

        public UserInfo()
        {

        }

        public void Set(CurrentUser user)
        {
            if (User is null)
            {
                User = user;
                IsAuthorize = true;
            }
        }

        public void Clean(CurrentUser user)
        {
            if (User != null)
            {
                User = null;
                IsAuthorize = false;
            }
        }
    }


}
