﻿using DataAccessLayerAbstraction.Mapping;
using DataAccessLayerAbstraction.Mapping.Types;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Http.Features;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using System;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebApi.Auth.Attributes;
using WebApi.Auth.Models;
using WebApi.Auth.Settings;

namespace WebApi.Auth
{
    public class AuthorizationMiddleware
    {
        private readonly AuthSettings _authSettings;
        private readonly RequestDelegate _next;

        public AuthorizationMiddleware(IOptions<AuthSettings> authSettings, RequestDelegate next)
        {
            _next = next;
            _authSettings = authSettings.Value ?? throw new ArgumentNullException(nameof(authSettings));
        }

        public async Task Invoke(HttpContext context, IUserInfo userInfo)
        {
            var endpoint = context.Features.Get<IEndpointFeature>()?.Endpoint;
            var authAttr = endpoint?.Metadata.GetMetadata<AuthAttribute>();
            var noneAuth = endpoint?.Metadata.GetMetadata<AllowNotAuthAttribute>();

            if (authAttr == null)
            {
                await _next(context);

                return;
            }

            if (noneAuth != null)
            {
                await _next(context);

                return;
            }

            var token = context.Request.Headers["Authorization"].FirstOrDefault()?.Split(" ").Last();

            if (string.IsNullOrEmpty(token) == false)
            {
                AttachUserToContext(context, token, userInfo);
            }

            if (userInfo.IsAuthorize == false || authAttr.Roles.Count > 0 && authAttr.Roles.Contains(userInfo.User.Role) == false)
            {
                context.Response.Clear();
                context.Response.StatusCode = (int)StatusCodes.Status401Unauthorized;
                await context.Response.WriteAsync("Unauthorized");

                return;
            }

            await _next(context);
        }

        private void AttachUserToContext(HttpContext context, string token, IUserInfo userInfo)
        {
            try
            {
                var tokenHandler = new JwtSecurityTokenHandler();
                var key = Encoding.ASCII.GetBytes(_authSettings.JwtSecret);

                tokenHandler.ValidateToken(token, new TokenValidationParameters
                {
                    ValidateIssuerSigningKey = true,
                    IssuerSigningKey = new SymmetricSecurityKey(key),
                    ValidateIssuer = false,
                    ValidateAudience = false,
                    ClockSkew = TimeSpan.Zero  // set clockSkew to zero so tokens expire exactly at token expiration time (instead of 5 minutes later)
                }, out SecurityToken validatedToken);


                var jwtToken = (JwtSecurityToken)validatedToken;
                var userId = jwtToken.Claims.First(x => x.Type == nameof(Account.Id)).Value;
                var role = jwtToken.Claims.First(x => x.Type == nameof(Account.Role)).Value;
                Role useRole = Enum.Parse<Role>(role);

                userInfo.Set(new CurrentUser()
                {
                    UserId = userId,
                    Role = useRole,
                });
            }
            catch
            {
                // do nothing if jwt validation fails
                // user is not attached to context so request won't have access to secure routes
            }
        }
    }
}
