﻿using DataAccessLayerAbstraction;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using WebApi.Auth.Models;
using WebApi.Auth.Settings;


namespace WebApi.Auth
{
    public interface IAuthenticationService
    {
        Task<string> Authorize(UserLoginModel userLoginModel, TimeSpan tokenLifeTime);
    }

    public class AuthenticationService : IAuthenticationService
    {
        private readonly AuthSettings _authSettings;
        private readonly IDataAccessLayer _dal;
        private readonly IPasswordHasher _passwordHasher;

        public AuthenticationService(
            IOptions<AuthSettings> authSettings,
            IPasswordHasher passwordHasher,
            IDataAccessLayer dataAccessLayer)
        {
            _authSettings = authSettings.Value ?? throw new ArgumentNullException(nameof(authSettings.Value));
            _passwordHasher = passwordHasher ?? throw new ArgumentNullException(nameof(passwordHasher));
            _dal = dataAccessLayer ?? throw new ArgumentNullException(nameof(dataAccessLayer));
        }

        /// <returns>Return token or null if unsuccess</returns>
        public async Task<string> Authorize(UserLoginModel userLoginModel, TimeSpan tokenLifeTime)
        {
            var currentUser = await _dal.Account.GetByLogin(userLoginModel.Login);

            if (currentUser is null)
            {
                return null;
            }

            if (_passwordHasher.VerifyHashedPassword(currentUser.Password, userLoginModel.Password) == PasswordVerificationResult.Failed)
            {
                return null;
            }

            List<Claim> payloads = new List<Claim>
            {
                new Claim(nameof(currentUser.Id), currentUser.Id.ToString()),
                new Claim(nameof(currentUser.Role), currentUser.Role.ToString())
            };

            return GenerateJwtToken(payloads, tokenLifeTime);
        }

        private string GenerateJwtToken(List<Claim> payloads, TimeSpan tokenLifeTime)
        {
            var tokenHandler = new JwtSecurityTokenHandler();
            var key = Encoding.ASCII.GetBytes(_authSettings.JwtSecret);

            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(payloads),
                Expires = DateTime.UtcNow + tokenLifeTime,
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature)
            };

            var token = tokenHandler.CreateToken(tokenDescriptor);

            return tokenHandler.WriteToken(token);
        }
    }
}
