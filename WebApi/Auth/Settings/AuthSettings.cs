﻿namespace WebApi.Auth.Settings
{
    public class AuthSettings
    {
        public string JwtSecret { get; set; }
        public HashingOptions PasswordHashingOptions { get; set; }
    }
}
