﻿namespace WebApi.Auth.Settings
{
    public sealed class HashingOptions
    {
        public int Iterations { get; set; } = 10000;
    }
}
