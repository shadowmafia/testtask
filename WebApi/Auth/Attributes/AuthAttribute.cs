﻿using DataAccessLayerAbstraction.Mapping.Types;
using System;
using System.Collections.Generic;

namespace WebApi.Auth.Attributes
{
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method)]
    public class AuthAttribute : Attribute
    {
        public List<Role> Roles { get; set; }

        public AuthAttribute()
        {
            Roles = new List<Role>();
        }

        public AuthAttribute(Role role)
        {
            Roles = new List<Role>()
            {
                role
            };
        }

        public AuthAttribute(params Role[] parameters)
        {
            Roles = new List<Role>(parameters);
        }
    }
}
