﻿using System;

namespace WebApi.Auth.Attributes
{
    [AttributeUsage(AttributeTargets.Method)]
    public class AllowNotAuthAttribute : Attribute
    {
        public AllowNotAuthAttribute() { }
    }
}
