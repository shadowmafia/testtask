﻿using DataAccessLayerAbstraction.Repositories;

namespace DataAccessLayerAbstraction
{
    public interface IDataAccessLayer
    {
        IAccountRepository Account { get; }
        IEmployeeRepository Employee { get; }
        IJobRepository Job { get; }
        IProjectRepository Project { get; }
    }
}
