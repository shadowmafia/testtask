﻿using System.Threading.Tasks;

namespace DataAccessLayerAbstraction
{
    public interface IBaseRepository<T> where T : class
    {
        /// <returns>return id if was created new object</returns>
        Task<int> Create(T obj);

        Task Update(T obj);

        Task Delete(int id);

        /// <summary>
        ///  return null if object not found.
        /// </summary>
        Task<T> GetById(int id);
    }
}
