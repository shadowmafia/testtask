﻿using System;

namespace DataAccessLayerAbstraction.Mapping
{
    public class Project
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? ReleaseDate { get; set; }
        public int EmployeesCount { get; set; }
    }
}
