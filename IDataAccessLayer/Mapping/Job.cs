﻿namespace DataAccessLayerAbstraction.Mapping
{
    public class Job
    {
        public int Id { get; set; }
        public string Title { get; set; }
    }
}
