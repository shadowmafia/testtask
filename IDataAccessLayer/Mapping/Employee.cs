﻿using System;

namespace DataAccessLayerAbstraction.Mapping
{
    public class Employee
    {
        public int Id { get; set; }
        public int? AccountId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public DateTime? EmploymentDate { get; set; }
        public Job Job { get; set; }
    }
}
