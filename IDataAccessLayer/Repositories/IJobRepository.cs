﻿using DataAccessLayerAbstraction.Mapping;

namespace DataAccessLayerAbstraction.Repositories
{
    public interface IJobRepository : IBaseRepository<Job> { }
}
