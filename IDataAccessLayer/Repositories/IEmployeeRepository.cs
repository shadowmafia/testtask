﻿using DataAccessLayerAbstraction.Mapping;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace DataAccessLayerAbstraction.Repositories
{
    public interface IEmployeeRepository : IBaseRepository<Employee>
    {
        Task<List<Project>> GetProjects(int employeeId);
        Task<List<Employee>> GetEmployes();
    }
}
