﻿using DataAccessLayerAbstraction.Mapping;
using DataAccessLayerAbstraction.Mapping.Types;
using System.Threading.Tasks;

namespace DataAccessLayerAbstraction.Repositories
{
    public interface IAccountRepository
    {
        Task<Account> GetByLogin(string login);
        Task<int> Create(Account obj);
        Task Delete(int id);
        Task UpdatePassword(int accountId, string password);
        Task UpdateRole(int accountId, Role role);
        /// <summary>
        ///  return null if object not found.
        /// </summary>
        Task<Account> GetById(int id);
    }
}
