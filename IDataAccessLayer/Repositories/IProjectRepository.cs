﻿using DataAccessLayerAbstraction.Mapping;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace DataAccessLayerAbstraction.Repositories
{
    public interface IProjectRepository : IBaseRepository<Project>
    {
        Task<List<Project>> GetProjects();

        Task AddEmployee(int projectId, int employeeId);
        Task RemoveEmployee(int projectId, int employeeId);

        Task<List<Employee>> GetEmployes(int projectId);
    }
}
