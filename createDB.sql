CREATE TABLE Account(
  ID INTEGER GENERATED BY DEFAULT AS IDENTITY PRIMARY KEY,
  login VARCHAR(50) NOT NULL,
  PASSWORD CHAR(73) NOT NULL,
  role CHAR NOT NULL,
  UNIQUE (login)
);

CREATE TABLE Job(
  ID INTEGER GENERATED BY DEFAULT AS IDENTITY PRIMARY KEY,
  title VARCHAR(50) NOT NULL,
  UNIQUE (title)
);

CREATE TABLE Employee(
  ID INTEGER GENERATED BY DEFAULT AS IDENTITY PRIMARY KEY,
  firstName VARCHAR(50) NOT NULL,
  lastName VARCHAR(50) NOT NULL,
  employmentDate DATE NOT NULL,
  job INTEGER,
  accountId INTEGER,
  FOREIGN KEY(job) REFERENCES Job(ID) ON DELETE SET DEFAULT,
  FOREIGN KEY (accountId) REFERENCES Account(id) ON DELETE SET NULL,
  UNIQUE (firstName,lastName)
);

CREATE TABLE Project(
  ID INTEGER GENERATED BY DEFAULT AS IDENTITY PRIMARY KEY,
  title VARCHAR(50) NOT NULL,
  description TEXT NOT NULL,
  startDate DATE NOT NULL CHECK (releaseDate > '2000-01-01'),
  releaseDate DATE NOT NULL CHECK (releaseDate > startDate)
);

CREATE TABLE ProjectEmployee(
	projectId INTEGER NOT NULL,
	employeeId INTEGER NOT NULL,
	FOREIGN KEY(projectId) REFERENCES Project(ID) ON DELETE CASCADE,
    FOREIGN KEY(employeeId) REFERENCES Employee(ID) ON DELETE CASCADE,
	UNIQUE (projectId,employeeId)
);

--unhashed password->Admin
INSERT INTO Account (login,password,role)
VALUES('admin','777.K8EcAmHAIL4OtHdwK5H+ZQ==.sFojb9DaJ2BuBkMFzr5QOw2TALUmdfUGE+N6a5+XijI=',1);

INSERT INTO Job (title)
VALUES
('Jon Developer'),
('Frontend Developer'),
('Designer'),
('Tester');

INSERT INTO Employee (firstName,lastName,employmentDate,job,accountId)
VALUES
('John','Doe','2019-01-02',1,1),
('Jane','Doe','2020-07-14',2,NULL),
('Valera','Vetrov','2020-03-12',3,NULL);

INSERT INTO Project (title, description, startDate, releaseDate)
VALUES
('AliExpress','marketplace','2019-01-02','2020-01-02'),
('World Of Warcraft Shadowlands','MMORPG','2017-01-02','2020-09-14');

INSERT INTO ProjectEmployee (projectId,employeeId)
VALUES
(1,1),
(2,1),
(2,2),
(2,3);